package com.example.tp1animal;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class AnimalActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animal_activity);

        Intent intent = getIntent();
        String nomAnimal = "";
        if (intent.hasExtra("nomAnimal")) // vérifie qu'une valeur est associée à la clé “”
            nomAnimal = intent.getStringExtra("nomAnimal"); // on récupère la valeur associée à la clé

        final Animal animal = AnimalList.getAnimal(nomAnimal);

        final ImageView imageAnimal = findViewById(R.id.imageView);
        imageAnimal.setImageResource(getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName()));

        final TextView nom = findViewById(R.id.nom);
        nom.setText(nomAnimal);

        final TextView esperance = findViewById(R.id.valeurEspVie);
        esperance.setText(animal.getStrHightestLifespan());

        final TextView gestation = findViewById(R.id.valeurGestation);
        gestation.setText(animal.getStrGestationPeriod());

        final TextView  poidsEnfant = findViewById(R.id.valeurPoidsE);
        poidsEnfant.setText(animal.getStrBirthWeight());

        final TextView  poidsAdulte = findViewById(R.id.valeurPoidsA);
        poidsAdulte.setText(animal.getStrAdultWeight());

        final TextInputEditText conservation = findViewById(R.id.valStatutCons);
        conservation.setText(animal.getConservationStatus());


        Button bSave = (Button) findViewById(R.id.bSave);
        bSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                animal.setConservationStatus(conservation.getText().toString());
                finish();
            }
        });
    }
}