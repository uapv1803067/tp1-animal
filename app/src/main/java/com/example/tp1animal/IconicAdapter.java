package com.example.tp1animal;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class IconicAdapter extends RecyclerView.Adapter<IconicAdapter.RowHolder>
{
    public AnimalList animaux;
    public Context context;
    public OnItemClick selecteur;


    IconicAdapter(Context context)
    {
        this.context = context;
    }

    public static class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        OnItemClick onItemClick;
        TextView label;
        ImageView icon;

        RowHolder(View row, OnItemClick selecteur)
        {
            super(row);
            label=row.findViewById(R.id.nomAnimal);
            icon=row.findViewById(R.id.imageView);

            row.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            onItemClick.OnItemClick(getAdapterPosition());
            // Do something in response to the click
            //final String item = label.getText().toString();
            //Toast.makeText(v.getContext(), "you selected" + item, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public RowHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listanimaux,parent,false);

        return new RowHolder(view, selecteur);
        //return(new RowHolder(getLayoutInflater()
        //.inflate(R.layout.animal_activity, parent, false)));
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder rowHolder, int position)
    {
        String cle = (String) animaux.getNameArray()[position];
        Animal animal = animaux.getAnimal(cle);
        String imgAnimal = animal.getImgFile();

        int animalID = context.getResources().getIdentifier(imgAnimal , "drawable", context.getPackageName());
        rowHolder.icon.setImageResource(animalID);;
        rowHolder.label.setText(cle);
        //String[] items;
        //RowHolder.bindModel(items[position]);
    }

    @Override
    public int getItemCount()
    {
        return animaux.getNameArray().length;
    }

    public interface OnItemClick
    {
        void OnItemClick(int position);
    }
}