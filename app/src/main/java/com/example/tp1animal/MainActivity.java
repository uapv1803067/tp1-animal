package com.example.tp1animal;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity<publicRowHolder> extends AppCompatActivity {
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, AnimalList.getNameArray());

        ListView listView = (ListView) findViewById(R.id.lv);
        listView.setAdapter(itemsAdapter);

        intent = new Intent(this, AnimalActivity.class);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                // Do something in response to the click
                final String item = (String) parent.getItemAtPosition(position);
                intent.putExtra("nomAnimal", item);
                startActivity(intent);
        //initRecycleView();
    }
/*
    private void initRecycleView() {
        RecyclerView recyclerView = findViewById(R.id.recycler);
        IconicAdapter adapter = new IconicAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }*/
}
        );}}

